#pragma once

#include "physics.h"

namespace gurgula
{

	struct SubObject
	{
		Polygon pol;
		WorldCoordinate relativePosition;
		Radian relativeAngle;
		b2Fixture* fixture;
	};
	
	struct Object
	{
		std::vector<SubObject> objects;
		Physics::PositionTracker body;
	};

	class Simulation
	{
	public:
		Simulation() : mPhysics(50, 50), shits(1)
		{
			tri = { { -0.5, -.5 }, { .5, .5 }, { .5, -.5 } };

			for (int i(0); i < 5; ++i)
			{
				shits[0].objects.push_back({ tri, { 0, i }, Radian() });
			}
			for (int i(0); i < 5; ++i)
			{
				shits[0].objects.push_back({ tri, { 2, i }, Radian() });
			}

			mPhysics.spawnObject(shits[0], { 0, 0 }, Radian());
			shits = mPhysics.breakObject(shits[0], { {0, 1, 2, 3, 4}, {5, 6, 7, 8, 9} });
		}

		std::vector<Object> getShit()
		{
			mPhysics.step(0.05);
			return shits;
		}

	private:
		Polygon tri;
		std::vector<Object> shits;

		Physics mPhysics;
	};
}