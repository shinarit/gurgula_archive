#include <iostream>

#include "config.h"
#include "ui/ui.h"

int main(int argc, char* argv[])
{
	gurgula::Config cfg;
	gurgula::Ui ui(argc, argv);
	ui.run();
}
