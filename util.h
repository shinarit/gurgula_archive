#pragma once

#include <vector>
#include <cmath>

namespace gurgula
{
	typedef float WorldUnit;
	typedef int ScreenUnit;

	template <class Unit>
	struct vector
	{
		Unit x, y;
	};

	typedef vector<WorldUnit> WorldCoordinate;
	typedef vector<ScreenUnit> ScreenCoordinate;

	inline ScreenCoordinate WorldToScreen(const WorldCoordinate& coord)
	{
		return { coord.x, coord.y };
	}
	inline WorldCoordinate ScreenToWorld(const ScreenCoordinate& coord)
	{
		return{ coord.x, coord.y };
	}

	template <class Coordinate>
	float distance(const Coordinate& lhs, const Coordinate& rhs)
	{
		return std::sqrt((lhs.x - rhs.x) * (lhs.x - rhs.x) + (lhs.y - rhs.y) * (lhs.y - rhs.y));
	}

	struct Rectangle
	{
		ScreenCoordinate topLeft, bottomRight;
	};

	typedef std::vector<WorldCoordinate> Polygon;

	const double pi = 3.1415926535897;

	enum class MeasurementUnits
	{
		Radian,
		Degree
	};

	template <class BaseType, MeasurementUnits unit>
	struct MeasurementUnit
	{
		MeasurementUnit() : value()
		{}
		explicit MeasurementUnit(BaseType value) : value(value)
		{}

		operator BaseType()
		{
			return value;
		}

		BaseType value;
	};

	template <class BaseType, MeasurementUnits unit>
	MeasurementUnit<BaseType, unit> operator+(const MeasurementUnit<BaseType, unit>& lhs, const MeasurementUnit<BaseType, unit>& rhs)
	{
		return MeasurementUnit<BaseType, unit>(lhs.value + rhs.value);
	}

	typedef MeasurementUnit<float, MeasurementUnits::Radian> Radian;
	typedef MeasurementUnit<float, MeasurementUnits::Degree> Degree;

	inline Degree radianToDegree(Radian radian)
	{
		return Degree(radian.value / pi * 180.0);
	}


	inline WorldCoordinate rotate(const WorldCoordinate& vec, Radian angle)
	{
		float cos(std::cos(angle));
		float sin(std::sin(angle));

		return{vec.x * cos - vec.y * sin, vec.x * sin + vec.y * cos};
	}
}
