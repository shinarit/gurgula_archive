#include "graphics.h"

#include <glfw/glfw3.h>

gurgula::Graphics::Graphics()
{
	theGraphics = this;
	if (GL_FALSE == glfwInit())
		throw - 1;

	GLFWmonitor* monitor(glfwGetPrimaryMonitor());
	const GLFWvidmode* mode(glfwGetVideoMode(monitor));
	mSize = { mode->width, mode->height };

	createWindow();
}

gurgula::Graphics::Graphics(ScreenCoordinate size) : mSize(size)
{
	theGraphics = this;
	if (GL_FALSE == glfwInit())
		throw -1;

	createWindow();
}

gurgula::Graphics::~Graphics()
{
	glfwTerminate();
}

gurgula::Graphics::Time gurgula::Graphics::getTime()
{
	return glfwGetTime();
}

void gurgula::Graphics::drawShit()
{
	float size = .5;
	/*
	glBegin(GL_QUADS);
	glColor3f(1.f, 0.f, 0.f);
	glNormal3f(0, 0, 1);
	glVertex3f(0, 0, 0);          // top right of the quad (top)
	glVertex3f(-size, 0, 0);          // top left of the quad (top)
	glVertex3f(-size, size, 0);          // bottom left of the quad (top)
	glVertex3f(0, size, 0);          // bottom right of the quad (top)
	glEnd();
	*/

	vector<float> P0{ 0, 0 };
	vector<float> P1{ size, 0 };
	vector<float> P2{ size, size };
	vector<float> P3{ 0, size };
	vector<float> P4{ -1, -1 };
	vector<float> P5{ 1, 1 };
	vector<float> P6{ -1, 1 };
	vector<float> P7{ 1, -1 };

	glBegin(GL_LINES);
	glColor3f(0.f, 1.f, 0.f);
	glVertex2f(P0.x, P0.y);
	glVertex2f(P1.x, P1.y);
	glColor3f(0.f, 0.f, 1.f);
	glVertex2f(P2.x, P2.y);
	glVertex2f(P3.x, P3.y);
	glColor3f(1.f, 1.f, 0.f);
	glVertex2f(P4.x, P4.y);
	glVertex2f(P5.x, P5.y);
	glColor3f(1.f, 0.f, 1.f);
	glVertex2f(P6.x, P6.y);
	glVertex2f(P7.x, P7.y);
	glEnd();
}

void gurgula::Graphics::drawBox(const Rectangle& box)
{
	OpenGlScreenCoordinate topLeft(translatePixelToDrawScreenCoordinate(box.topLeft));
	OpenGlScreenCoordinate bottomRight(translatePixelToDrawScreenCoordinate(box.bottomRight));

	glPushMatrix();
	glLoadIdentity();

	glBegin(GL_LINE_LOOP);
	glColor3f(0.f, 1.f, 0.f);
	glVertex2f(topLeft.x, topLeft.y);
	glVertex2f(bottomRight.x, topLeft.y);
	glColor3f(0.f, 0.f, 1.f);
	glVertex2f(bottomRight.x, bottomRight.y);
	glVertex2f(topLeft.x, bottomRight.y);
	glEnd();

	glPopMatrix();
}

void gurgula::Graphics::drawPolygon(const Polygon& pol, WorldCoordinate position, float rotate, float scale)
{
	OpenGlScreenCoordinate scrPossition(translatePixelToDrawScreenCoordinate(position));

	glPushMatrix();

	glTranslatef(scrPossition.x, scrPossition.y, 0);
	glRotatef(rotate, 0, 0, 1);
	glScalef(scale, scale, 1);
	glTranslatef(1, 1, 0);

	glColor3f(0.f, 1.f, 0.f);

	drawPolygon(pol);

	glPopMatrix();
}

void gurgula::Graphics::startUpdate()
{
	glfwGetWindowSize(mWindow, &mSize.x, &mSize.y);
	glViewport(0, 0, mSize.x, mSize.y);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glDisable(GL_DEPTH_TEST);
}

void gurgula::Graphics::finishUpdate()
{
	glfwSwapBuffers(mWindow);
}

void gurgula::Graphics::handleInput()
{
	glfwPollEvents();
}

void gurgula::Graphics::registerInputHandler(EventCallback callback)
{
	mEventCallback = callback;
}











void gurgula::Graphics::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (nullptr != theGraphics->mEventCallback && GLFW_REPEAT != action)
	{
		Event event;
		event.type = EventType::KEYBOARD;
		//event.keyboardPressed = KeyboardEventDetails{ scancode, ((GLFW_PRESS == action) ? ButtonStatus::PRESSED : ButtonStatus::RELEASED) };
		event.keyboardPressed = KeyboardEventDetails{ key, ((GLFW_PRESS == action) ? ButtonStatus::PRESSED : ButtonStatus::RELEASED) };
		theGraphics->mEventCallback(event);
	}
}

void gurgula::Graphics::cursorPositionCallback(GLFWwindow* window, double xpos, double ypos)
{
	if (nullptr != theGraphics->mEventCallback)
	{
		Event event;
		event.type = EventType::MOUSE_MOVED;
		event.mouseMoved = MouseMovedEventDetails{MousePosition{ xpos, ypos }};
		theGraphics->mEventCallback(event);
	}
}

void gurgula::Graphics::mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	if (nullptr != theGraphics->mEventCallback)
	{
		Event event;
		event.type = EventType::MOUSE_PRESSED;
		event.mousePressed = MousePressedEventDetails{button, ((GLFW_PRESS == action) ? ButtonStatus::PRESSED : ButtonStatus::RELEASED)};
		theGraphics->mEventCallback(event);
	}
}

void gurgula::Graphics::drawPolygon(const Polygon& pol)
{
	glBegin(GL_LINE_LOOP);

	for (const WorldCoordinate& coord : pol)
	{
		OpenGlScreenCoordinate drawCoord(translatePixelToDrawScreenCoordinate(coord));
		glVertex2f(drawCoord.x, drawCoord.y);
	}

	glEnd();
}

void gurgula::Graphics::createWindow()
{
	//glfwWindowHint(GLFW_DOUBLEBUFFER, 1);

	//borderless fullscreen
	glfwWindowHint(GLFW_RESIZABLE, 0);
	glfwWindowHint(GLFW_DECORATED, 0);
	GLFWmonitor* monitor(glfwGetPrimaryMonitor());
	const GLFWvidmode* mode(glfwGetVideoMode(monitor));
	mWindow = glfwCreateWindow(mSize.x, mSize.y, "PENIS",
		//		monitor,
		0,
		0);

	glfwMakeContextCurrent(mWindow);

	glfwSwapInterval(1);

	glfwSetKeyCallback(mWindow, keyCallback);
	glfwSetCursorPosCallback(mWindow, cursorPositionCallback);
	glfwSetMouseButtonCallback(mWindow, mouseButtonCallback);
}

gurgula::Graphics::OpenGlScreenCoordinate gurgula::Graphics::translatePixelToDrawScreenCoordinate(WorldCoordinate coord)
{
	return { float(coord.x - mSize.x / 2) / mSize.x * 2, float(coord.y - mSize.y / 2) / mSize.y * 2 };
}

gurgula::Graphics::OpenGlScreenCoordinate gurgula::Graphics::translatePixelToDrawScreenCoordinate(ScreenCoordinate coord)
{
	return translatePixelToDrawScreenCoordinate(WorldCoordinate{coord.x, coord.y});
}


gurgula::Graphics* gurgula::Graphics::theGraphics = 0;




