#pragma once

#include "util.h"

namespace gurgula
{
	class Graphics;
	class Simulation;
	class Event;
	class Observer
	{
	public:
		Observer(Graphics& graphics, Simulation& simulation);
		void draw();
		void handleInput(const Event& event);

	private:
		Graphics& mGraphics;
		Simulation& mSimulation;
		ScreenCoordinate mMousePos;
		WorldCoordinate mObservedPoint;
	};
}
