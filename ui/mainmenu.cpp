#include "mainmenu.h"

#include "../graphics.h"
#include "gamewindow.h"

gurgula::MainMenu::MainMenu(Graphics& graphics) : Screen(graphics)
{}

void gurgula::MainMenu::draw()
{
	mGraphics.drawBox(Rectangle{ { 20, 20 }, { 100, 100 } });
}

gurgula::Screen::ScreenPtr gurgula::MainMenu::transition(TransitionState& backStep)
{
	switch (backStep)
	{
	case TransitionState::NOP:
		if (add)
		{
			add = false;
			return ScreenPtr(new GameWindow(mGraphics));
		}
		break;
	case TransitionState::EXIT:
		break;
	default:
		backStep = TransitionState::NOP;
		break;
	}

	ScreenPtr ptr;
	return ptr;
}

#include <iostream>
void gurgula::MainMenu::handleInput(const Event& event)
{
	if (event.type == EventType::KEYBOARD)
	{
		exit = true;
	}
	if (event.type == EventType::MOUSE_PRESSED)
	{
		add = true;
	}
	std::cerr << "penis erkezett " << int(event.type) << "\n";
}

float gurgula::MainMenu::getFramerate() const
{
	return 30.0;
}

float gurgula::MainMenu::getTickrate() const
{
	return 0.0;
}
