#pragma once

#include "../graphics.h"

namespace gurgula
{
	class Event;

	class Ui
	{
	public:
		Ui(int argc, char* argv[]);
		void run();

	private:
		void handleInput(const Event& event);
		Graphics mMainWindow;
	};
}
