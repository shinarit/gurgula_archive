#pragma once

#include <memory>

#include "../util.h"
#include "event.h"

namespace gurgula
{
	class Graphics;

	class Screen
	{
	public:
		enum class TransitionState
		{
			NOP,
			EXIT
		};
		typedef std::unique_ptr<Screen> ScreenPtr;


		Screen(Graphics& graphics) : mGraphics(graphics)
		{}
		virtual ~Screen()
		{}

		virtual void draw() = 0;
		virtual void tick()
		{}
		virtual ScreenPtr transition(TransitionState& backStep) = 0;
		virtual void handleInput(const Event& event) = 0;
		virtual float getFramerate() const = 0;
		virtual float getTickrate() const = 0;

	protected:
		Graphics& mGraphics;
	};
	/*
	class SubScreen
	{
	public:
		SubScreen(Coordinate offset) : offset(offset)
		{}
		virtual ~SubScreen()
		{}
	private:
		Coordinate offset;
	};

	class CompoundScreen : public Screen
	{

	};
	*/
}
