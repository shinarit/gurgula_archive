#pragma once

#include "screen.h"
#include "../observer.h"
#include "../simulation.h"

#include <vector>

namespace gurgula
{
	class GameWindow : public Screen
	{
	public:
		GameWindow(Graphics& graphics/*std::vector<Controller*>*/);
		virtual void draw();
		virtual ScreenPtr transition(TransitionState& backStep);
		virtual void handleInput(const Event& event);
		virtual float getFramerate() const;
		virtual float getTickrate() const;

	private:
		Simulation mSimulation;
		Observer mObserver;
		bool exit = false;
	};
}
