#pragma once

namespace gurgula
{
	enum class EventType
	{
		MOUSE_PRESSED, MOUSE_MOVED, KEYBOARD
	};

	enum class ButtonStatus
	{
		PRESSED, RELEASED
	};
	typedef int MouseButtonId;

	struct MousePressedEventDetails
	{
		MouseButtonId button;
		ButtonStatus status;
	};

	typedef vector<double> MousePosition;
	struct MouseMovedEventDetails
	{
		MousePosition cursor;
	};

	typedef int KeyCode;
	struct KeyboardEventDetails
	{
		KeyCode code;
		ButtonStatus status;
	};

	struct Event
	{
		EventType type;
		union
		{
			MousePressedEventDetails mousePressed;
			MouseMovedEventDetails mouseMoved;
			KeyboardEventDetails keyboardPressed;
		};
	};
}
