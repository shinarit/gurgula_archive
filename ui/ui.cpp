#include "ui.h"


#include <vector>
#include <utility>

#include "screen.h"
#include "mainmenu.h"
#include "event.h"

gurgula::Ui::Ui(int argc, char* argv[]) : mMainWindow(ScreenCoordinate{ 800, 600 })
{ }

void registerInputhandler(gurgula::Graphics& window, gurgula::Screen* screen)
{
	window.registerInputHandler(std::bind(&gurgula::Screen::handleInput, screen, std::placeholders::_1));
}

#include <iostream>
void gurgula::Ui::run()
{
	std::vector<Screen::ScreenPtr> screenStack;
	screenStack.push_back(Screen::ScreenPtr(new MainMenu(mMainWindow)));

	bool screenChanged(true);
	Graphics::Time timeOfLastFrameUpdate(0);
	Graphics::Time timeOfLastLogicUpdate(0);
	Screen::TransitionState transState(Screen::TransitionState::NOP);
	Graphics::Time frameUpdateDelay(1 / screenStack.back()->getFramerate());
	Graphics::Time logicUpdateDelay(1 / screenStack.back()->getTickrate());

	while (!screenStack.empty())
	{
		//std::cerr << "Screenstack: " << screenStack.size() << '\n';
		Screen& currentScreen(*screenStack.back());
		Screen::ScreenPtr nextScreen(currentScreen.transition(transState));
		if (nextScreen.get())
		{
			screenStack.push_back(std::move(nextScreen));
			screenChanged = true;
		}
		else if (Screen::TransitionState::NOP != transState)
		{
			screenStack.pop_back();
			screenChanged = true;
		}
		else
		{
			if (screenChanged)
			{
				registerInputhandler(mMainWindow, screenStack.back().get());
				frameUpdateDelay = 1 / screenStack.back()->getFramerate();
				logicUpdateDelay = screenStack.back()->getTickrate();
				screenChanged = false;
			}
			mMainWindow.handleInput();

			Graphics::Time currTime(mMainWindow.getTime());
			if (currTime - timeOfLastFrameUpdate > frameUpdateDelay)
			{
				mMainWindow.startUpdate();
				currentScreen.draw();
				mMainWindow.finishUpdate();

				timeOfLastFrameUpdate = currTime;
			}
			/*
			if (logicUpdateDelay > 0.0 && currTime - timeOfLastLogicUpdate > logicUpdateDelay)
			{
				currentScreen.tick();
				timeOfLastLogicUpdate = currTime;
			}
			*/
		}
	}
}
