#pragma once

#include "screen.h"

namespace gurgula
{
	class MainMenu : public Screen
	{
	public:
		MainMenu(Graphics& graphics);
		virtual void draw();
		virtual ScreenPtr transition(TransitionState& backStep);
		virtual void handleInput(const Event& event);
		virtual float getFramerate() const;
		virtual float getTickrate() const;

	private:
		bool exit = false;
		bool add = false;
	};
}
