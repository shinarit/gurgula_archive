#include "gamewindow.h"

#include "../graphics.h"

gurgula::GameWindow::GameWindow(Graphics& graphics) : Screen(graphics), mObserver(graphics, mSimulation)
{

}

void gurgula::GameWindow::draw()
{
	mObserver.draw();
}

gurgula::Screen::ScreenPtr gurgula::GameWindow::transition(TransitionState& backStep)
{
	if (exit)
	{
		backStep = TransitionState::EXIT;
	}
	return ScreenPtr();
}

void gurgula::GameWindow::handleInput(const Event& event)
{
	if (EventType::KEYBOARD == event.type)
	{
		if (int(Graphics::KeyConstant::ESCAPE) == event.keyboardPressed.code)
		{
			exit = true;
		}
	}
	mObserver.handleInput(event);
}

float gurgula::GameWindow::getFramerate() const
{
	return 30.0;
}

float gurgula::GameWindow::getTickrate() const
{
	return 30.0;
}
