#include "physics.h"

#include "simulation.h"

gurgula::Physics::Physics(float width, float height) : mWorld(b2Vec2(0., 1))
{
	mMap = { { -width / 2, -height / 2 }, { -width / 2, height / 2 }, { width / 2, height / 2 }, { width / 2, -height / 2 } };

	b2BodyDef groundBodyDef;
	b2PolygonShape groundBox;
	float largerDimension(std::max(width, height));
	groundBox.SetAsBox(1, largerDimension / 2);

	groundBodyDef.position.Set(-width / 2 - 1, 0);
	boundaries[0] = mWorld.CreateBody(&groundBodyDef);
	boundaries[0]->CreateFixture(&groundBox, 0.0f);

	groundBodyDef.position.Set(width / 2 + 1, 0);
	boundaries[1] = mWorld.CreateBody(&groundBodyDef);
	boundaries[1]->CreateFixture(&groundBox, 0.0f);

	groundBodyDef.angle = pi / 2;
	groundBodyDef.position.Set(0, -height / 2 - 1);
	boundaries[2] = mWorld.CreateBody(&groundBodyDef);
	boundaries[2]->CreateFixture(&groundBox, 0.0f);

	groundBodyDef.position.Set(0, height / 2 + 1);
	boundaries[3] = mWorld.CreateBody(&groundBodyDef);
	boundaries[3]->CreateFixture(&groundBox, 0.0f);
}


gurgula::Physics::~Physics()
{}

void gurgula::Physics::step(float time)
{
	mWorld.Step(time, velocityIterations, positionIterations);
}

const gurgula::Polygon& gurgula::Physics::map() const
{
	return mMap;
}

void gurgula::Physics::spawnObject(Object& object, WorldCoordinate position, Radian angle)
{
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;

	bodyDef.position.Set(position.x, position.y);
	bodyDef.angle = angle;
	object.body.mBody = mWorld.CreateBody(&bodyDef);

	b2PolygonShape dynamicBox;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &dynamicBox;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.3f;

	B2Polygon b2p;
	for (SubObject& subObj : object.objects)
	{
		b2p = createB2PolygonForSubObject(subObj);
		dynamicBox.Set(b2p.data(), b2p.size());
		subObj.fixture = object.body.mBody->CreateFixture(&fixtureDef);
	}
}

std::vector<gurgula::Object> gurgula::Physics::breakObject(Object& object, std::vector<std::vector<int>> groups)
{
	std::vector<Object> result;

	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;

	bodyDef.position = object.body.mBody->GetPosition();
	bodyDef.angle = object.body.mBody->GetAngle();

	b2FixtureDef fixtureDef;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.3f;

	for (const auto& group : groups)
	{
		Object obj;

		obj.body.mBody = mWorld.CreateBody(&bodyDef);
		for (int index : group)
		{
			obj.objects.push_back(object.objects[index]);
			fixtureDef.shape = obj.objects.back().fixture->GetShape();
			obj.objects.back().fixture = obj.body.mBody->CreateFixture(&fixtureDef);
		}
		
		result.push_back(obj);
	}

	//cleanup
	mWorld.DestroyBody(object.body.mBody);
	
	return result;
}

gurgula::Physics::B2Polygon gurgula::Physics::createB2PolygonForSubObject(const SubObject& subObj)
{
	B2Polygon res;
	for (const WorldCoordinate& vertex : subObj.pol)
	{
		WorldCoordinate rotatedVertex(rotate(vertex, subObj.relativeAngle));
		res.push_back({ rotatedVertex.x + subObj.relativePosition.x, rotatedVertex.y + subObj.relativePosition.y });
	}
	return res;
}
