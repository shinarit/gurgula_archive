#pragma once

#include <functional>

#include "util.h"
#include "ui/event.h"
class GLFWwindow;

namespace gurgula
{
	class Graphics
	{
	public:
		enum class KeyConstant
		{
			ESCAPE = 256
		};
		typedef std::function<void(const Event&)> EventCallback;
		//time in seconds
		typedef double Time;

		Graphics();
		Graphics(ScreenCoordinate size);
		~Graphics();
		ScreenCoordinate size() const
		{
			return mSize;
		}
		Time getTime();

		void drawShit();
		void drawBox(const Rectangle& box);
		void drawPolygon(const Polygon& pol, WorldCoordinate position, float rotate = 0.0, float scale = 1.0);

		void startUpdate();
		void finishUpdate();
		void handleInput();

		void registerInputHandler(EventCallback callback);

	private:
		typedef vector<double> OpenGlScreenCoordinate;

		static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void cursorPositionCallback(GLFWwindow* window, double xpos, double ypos);
		static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);

		void drawPolygon(const Polygon& pol);
		void createWindow();

		OpenGlScreenCoordinate translatePixelToDrawScreenCoordinate(WorldCoordinate coord);
		OpenGlScreenCoordinate translatePixelToDrawScreenCoordinate(ScreenCoordinate coord);

		static Graphics* theGraphics;

		ScreenCoordinate mSize;
		GLFWwindow* mWindow;
		EventCallback mEventCallback;
	};
}
