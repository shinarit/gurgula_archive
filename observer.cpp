#include "observer.h"

#include "graphics.h"
#include "simulation.h"
#include "ui/event.h"

gurgula::Observer::Observer(Graphics& graphics, Simulation& simulation) : mGraphics(graphics), mSimulation(simulation)
{
	mMousePos = { mGraphics.size().x / 2, mGraphics.size().y / 2 };
	mObservedPoint = { 0, 0 };
}

void gurgula::Observer::draw()
{
	std::vector<Object> shits(mSimulation.getShit());
	ScreenCoordinate size(mGraphics.size());
	float maxDistance(distance(ScreenCoordinate{ 0, 0 }, size) / 2);
	float scale((1 - (distance<WorldCoordinate>({ size.x / 2, size.y / 2 }, ScreenToWorld(mMousePos)) / maxDistance)) * 10 + 10);
	WorldCoordinate middle{ (mMousePos.x - size.x / 2) / scale + mObservedPoint.x, (-mMousePos.y + size.y / 2) / scale + mObservedPoint.y };


	WorldCoordinate botLeft{ middle.x - size.x / 2 / scale, middle.y - size.y / 2 / scale };

	for (const Object& shit : shits)
	{
		for (const SubObject& obj : shit.objects)
		{
			WorldCoordinate rotatedOffset(rotate(obj.relativePosition, Radian(shit.body.angle())));
			mGraphics.drawPolygon(obj.pol, { (shit.body.position().x + rotatedOffset.x - botLeft.x) * scale, (shit.body.position().y + rotatedOffset.y - botLeft.y) * scale }, radianToDegree(shit.body.angle() + obj.relativeAngle).value, scale);
		}
	}
}

void gurgula::Observer::handleInput(const Event& event)
{
	if (EventType::MOUSE_MOVED == event.type)
	{
		mMousePos = { event.mouseMoved.cursor.x, event.mouseMoved.cursor.y };
	}
}
