#pragma once

#include <Box2D/Box2D.h>
#include "util.h"

namespace gurgula
{
	class Object;
	class SubObject;

	class Physics
	{
	public:
		//creates a box with boundaries at -width/2 - +width/2 and -height/2 - +height/2
		Physics(float width, float height);
		~Physics();

		void step(float time);

		const Polygon& map() const;

		struct PositionTracker
		{
			WorldCoordinate position() const
			{
				return{ mBody->GetPosition().x, mBody->GetPosition().y };
			}
			Radian angle() const 
			{
				return Radian(mBody->GetAngle());
			}
			b2Body* mBody;
		};

		void spawnObject(Object& object, WorldCoordinate position, Radian angle);
		std::vector<gurgula::Object> breakObject(Object& object, std::vector<std::vector<int>> groups);
		
	private:
		typedef std::vector<b2Vec2> B2Polygon;

		B2Polygon createB2PolygonForSubObject(const SubObject& subObj);

		Polygon mMap;

		b2World mWorld;
		b2Body* boundaries[4];

		std::vector<b2Body*> bodies;


		static const int velocityIterations = 8;
		static const int positionIterations = 3;
	};
}